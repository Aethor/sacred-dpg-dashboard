# Scared

A dashboard for `Sacred` using `DearPyGui`.


# Installation

`pip install scared-dashboard`


# Usage

`python -m scared.dashboard`
