from typing import List, Literal


def _si_plot(*args):
    pass


def _si_set_grid(gridspec: Literal["1", "2x1", "2x2"], *_):
    from scared.dashboard import set_plot_grid

    set_plot_grid(gridspec)


SI_COMMAND_TABLE = {"plot": lambda *args: _si_plot, "set-grid": _si_set_grid}


def si_read(content: str) -> List[List[str]]:
    instructions = []
    for instruction_str in content.split(";"):
        command_and_args = instruction_str.split(" ")
        command_and_args = [e for e in command_and_args if not e == ""]
        instructions.append(command_and_args)
    return instructions


def si_eval(instructions: List[List[str]]):
    for instruction in instructions:
        command = instruction[0]
        args = instruction[1:]
        si_command = SI_COMMAND_TABLE.get(
            command, lambda *args: print(f"unknown command: {command}")
        )
        print(f"{command} {args}")
        si_command(*args)
